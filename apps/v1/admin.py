# -*- coding: utf-8 -*-

from django.conf import settings
from django.contrib import admin
from .models import (
Application,
Device,
User,
Question,
Answer,
QuizLotteryHistory,
QuizLotteryCount,
QuizAnswerHistory,
ItunesCode,
)
from .forms import (
QuestionForm,
AnswerForm,
)

import logging
logger = logging.getLogger('v1')

TIMESTAMP_FIELDS = (u'日時', {'fields': ['created_at', 'updated_at']})
DELETE_FIELDS = (u'論理削除', {
    'description': u'チェックするとアプリから非表示になります',
    'fields': ['is_deleted'],
})

class QuestionAdmin(admin.ModelAdmin):
    form = QuestionForm
    ordering = ('-id',)
    readonly_fields = ('created_at', 'updated_at')
    list_display = (
        'id', 'thumbnail_image_tag', 'content', 'lottery_rate',
        'winning_num', 'start_at', 'end_at', 'updated_at'
    )
    list_display_links = ('id', 'content',)
    fieldsets = [
        (None, {'fields': [
            'content', 'lottery_rate', 'time_limit', 'winning_num',
            'start_at', 'end_at'
        ]}),
        (u'画像', {'fields': [
            'thumbnail_image',
        ]}),
        TIMESTAMP_FIELDS,
        DELETE_FIELDS,
    ]
    list_select_related = True

    def thumbnail_image_tag(self, obj):
        file_path = obj.thumbnail_image
        if file_path is None or len(file_path) <= 0:
            return None
        return '<img src="%s%s" />' % (settings.MEDIA_URL, file_path)
    thumbnail_image_tag.allow_tags = True
    thumbnail_image_tag.short_description = u'サムネイル画像'

class AnswerAdmin(admin.ModelAdmin):
    form = AnswerForm
    ordering = ('-question', 'id',)
    readonly_fields = ('created_at', 'updated_at')
    list_display = (
        'id', 'question', 'content', 'is_correct',
        'created_at', 'updated_at'
    )
    list_display_links = ('id', 'content',)
    fieldsets = [
        (None, {'fields': [
            'question', 'content', 'is_correct', 'display_order',
        ]}),
        TIMESTAMP_FIELDS,
        DELETE_FIELDS,
    ]
    list_select_related = True

class ApplicationAdmin(admin.ModelAdmin):
    ordering = ('id',)
    readonly_fields = ('created_at', 'updated_at')
    list_display = (
        'id', 'name', 'client_key', 'secret_key',
        'created_at', 'updated_at'
    )
    list_display_links = ('id',)
    fieldsets = [
        (None, {'fields': [
            'name', 'client_key', 'secret_key', 'url_schema', 'appstore_url',
        ]}),
        TIMESTAMP_FIELDS,
    ]

class UserAdmin(admin.ModelAdmin):
    ordering = ('-id',)
    readonly_fields = ('created_at', 'updated_at')
    list_display = (
        'id',
    )
    list_display_links = ('id',)
    fieldsets = [
        (None, {'fields': []}),
        TIMESTAMP_FIELDS,
        DELETE_FIELDS,
    ]

class DeviceAdmin(admin.ModelAdmin):
    ordering = ('-id',)
    readonly_fields = ('created_at', 'updated_at')
    list_display = (
        'id', 'device_type', 'user', 'device_uuid',
        'created_at', 'updated_at'
    )
    list_display_links = ('id',)
    fieldsets = [
        (None, {'fields': [
            'user', 'device_uuid', 'device_type', 'session_id',
            'app_version', 'is_disabled_token',
        ]}),
        TIMESTAMP_FIELDS,
    ]

class QuizLotteryCountAdmin(admin.ModelAdmin):
    ordering = ('-id',)
    readonly_fields = ('created_at', 'updated_at')
    list_display = (
        'id', 'question', 'num',
    )
    list_display_links = ('id',)
    fieldsets = [
        (None, {'fields': [
            'question', 'num', 
        ]}),
        TIMESTAMP_FIELDS,
        DELETE_FIELDS,
    ]

class ItunesCodeAdmin(admin.ModelAdmin):
    ordering = ('-id',)
    readonly_fields = ('created_at', 'updated_at')
    list_display = (
        'id', 'code', 'is_deleted'
    )
    list_display_links = ('id',)
    fieldsets = [
        (None, {'fields': [
            'code', 
        ]}),
        TIMESTAMP_FIELDS,
        DELETE_FIELDS,
    ]

admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Application, ApplicationAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(QuizLotteryCount, QuizLotteryCountAdmin)
admin.site.register(ItunesCode, ItunesCodeAdmin)
