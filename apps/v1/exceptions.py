# -*- coding: utf-8 -*-

class QServerError(Exception):
    code = 'BASE_ERROR'
    http_status = 200

    def __init__(self, message, original=""):
        self.message = unicode(message)
        self.original = original

    def __unicode__(self):
        return self.message

    def get_error_object(self, debug=False):
        ret = {}
        ret['code'] = self.code
        ret['message'] = self.message

        if debug:
            ret['debug'] = unicode(self.original)

        return ret


class ValidationError(QServerError):
    code = 'VALIDATION_ERROR'
    http_status = 400


class ResourceNotFoundError(QServerError):
    code = 'RESOURCE_NOT_FOUND'
    http_status = 404


class PermissionError(QServerError):
    code = 'PERMISSION_ERROR'
    http_status = 403
