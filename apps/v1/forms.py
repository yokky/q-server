# -*- coding: utf-8 -*-

from django import forms
from .models import (
Question,
Answer,
)

# -----------------------------------------------------------------------------
# 設問
# -----------------------------------------------------------------------------
class QuestionForm(forms.ModelForm):
    content = forms.CharField(
        label=u'問題内容',
        help_text=u'最大{0}文字'.format(255),
        widget=forms.TextInput(attrs={'size': '130'}),
        max_length=255,
    )

    class Meta:
        model = Question
        fields = '__all__'

    # .. note:: How to override field value display in Django admin change form
    # http://stackoverflow.com/questions/11941971/how-to-override-field-value-display-in-django-admin-change-form
    def __init__(self, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.initial['content'] = self.instance.content

# -----------------------------------------------------------------------------
# 回答
# -----------------------------------------------------------------------------
class AnswerForm(forms.ModelForm):
    content = forms.CharField(
        label=u'回答内容',
        help_text=u'最大{0}文字'.format(100),
        widget=forms.TextInput(attrs={'size': '100'}),
        max_length=100,
    )

    class Meta:
        model = Answer
        fields = '__all__'

    # .. note:: How to override field value display in Django admin change form
    # http://stackoverflow.com/questions/11941971/how-to-override-field-value-display-in-django-admin-change-form
    def __init__(self, *args, **kwargs):
        super(AnswerForm, self).__init__(*args, **kwargs)
        self.initial['content'] = self.instance.content
