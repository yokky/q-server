# -*- coding: utf-8 -*-

from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from v1.service import (
question as question_service,
)
from requests_oauthlib import OAuth1Session

class Command(BaseCommand):
    def handle(self, *args, **options):
        #from_time = '20150906T000000'
        from_time = ''
	to_time = None
        if not from_time:
            now_datetime = datetime.now()
            from_time = now_datetime.strftime('%Y%m%dT%H%M%S')
        questions = question_service.get_questions(from_time, to_time)

        title = questions['questions'][0]['content']
        #title_tmpl = u"問題を解くだけでAmazonコードが当たる！さて問題です。{0}　#Amazon #クイ学 #クイズ #ウィズ #iPhoneAppJp #懸賞 気になったらDL→{1}"
        title_tmpl = u"さて問題です。{0}　#Amazon #クイ学 #クイズ #ウィズ #iPhoneJp #懸賞 気になったらDL→{1}"
        link = 'https://t.co/rFNkOpyQZP'

        self._tweet(title_tmpl.format(title, link))

        self.stdout.write('Successfully tweet question')

    def _tweet(self, title):
        CK = 'si8qw103RlEZCAnFL2VQ7slie'                             # Consumer Key
        CS = '0OPCSvFiahS9Li8rcheWoEVcMjO7lrMkIY8WlLa9fv5wpQjn87'    # Consumer Secret
        AT = '3189674174-bUBRUTktNRIitEuXwYzaFTAf6VzFgXU8CIxwdYM'    # Access Token
        AS = 'xKSm9pxqA5Ybg5DnLOzumFEd1LaQwn0bzJRHbExPw4wFi'         # Accesss Token Secert

        # ツイート投稿用のURL
        url = "https://api.twitter.com/1.1/statuses/update.json"

        # ツイート本文
        params = {"status": title}

        # OAuth認証で POST method で投稿
        twitter = OAuth1Session(CK, CS, AT, AS)
        req = twitter.post(url, params = params)

        # レスポンスを確認
        if req.status_code == 200:
            print ("OK")
        else:
            print ("Error: %d" % req.status_code)
