# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'v1_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('uuid', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'v1', ['User'])

        # Adding model 'Question'
        db.create_table(u'v1_question', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('thumbnail_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('content', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('lottery_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=9, decimal_places=6)),
            ('time_limit', self.gf('django.db.models.fields.IntegerField')()),
            ('winning_num', self.gf('django.db.models.fields.IntegerField')()),
            ('start_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_at', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'v1', ['Question'])

        # Adding model 'Answer'
        db.create_table(u'v1_answer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['v1.Question'])),
            ('content', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('display_order', self.gf('django.db.models.fields.IntegerField')()),
            ('is_correct', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'v1', ['Answer'])

        # Adding model 'QuizLotteryHistory'
        db.create_table(u'v1_quizlotteryhistory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['v1.User'])),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['v1.Question'])),
            ('itunes_code', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'v1', ['QuizLotteryHistory'])

        # Adding model 'QuizAnswerHistory'
        db.create_table(u'v1_quizanswerhistory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['v1.User'])),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['v1.Question'])),
            ('answer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['v1.Answer'])),
        ))
        db.send_create_signal(u'v1', ['QuizAnswerHistory'])


    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'v1_user')

        # Deleting model 'Question'
        db.delete_table(u'v1_question')

        # Deleting model 'Answer'
        db.delete_table(u'v1_answer')

        # Deleting model 'QuizLotteryHistory'
        db.delete_table(u'v1_quizlotteryhistory')

        # Deleting model 'QuizAnswerHistory'
        db.delete_table(u'v1_quizanswerhistory')


    models = {
        u'v1.answer': {
            'Meta': {'object_name': 'Answer'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'display_order': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_correct': ('django.db.models.fields.BooleanField', [], {}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.Question']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'v1.question': {
            'Meta': {'object_name': 'Question'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'end_at': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lottery_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '6'}),
            'start_at': ('django.db.models.fields.DateTimeField', [], {}),
            'thumbnail_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'time_limit': ('django.db.models.fields.IntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'winning_num': ('django.db.models.fields.IntegerField', [], {})
        },
        u'v1.quizanswerhistory': {
            'Meta': {'object_name': 'QuizAnswerHistory'},
            'answer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.Answer']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.Question']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.User']"})
        },
        u'v1.quizlotteryhistory': {
            'Meta': {'object_name': 'QuizLotteryHistory'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'itunes_code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.Question']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.User']"})
        },
        u'v1.user': {
            'Meta': {'object_name': 'User'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['v1']