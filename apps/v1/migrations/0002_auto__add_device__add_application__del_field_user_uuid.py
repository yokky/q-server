# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Device'
        db.create_table(u'v1_device', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='devices', to=orm['v1.User'])),
            ('device_uuid', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('device_type', self.gf('django.db.models.fields.SmallIntegerField')(max_length=1)),
            ('session_id', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('app_version', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('is_disabled_token', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'v1', ['Device'])

        # Adding model 'Application'
        db.create_table(u'v1_application', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('client_key', self.gf('django.db.models.fields.CharField')(default='3a4Jq_IjEeSE_2D4Ha4KqA', unique=True, max_length=64)),
            ('secret_key', self.gf('django.db.models.fields.CharField')(default=u'5k1idl3lblhcEf6aT6W8TlFcd0nOrHrNlFcU7UYcXF8YOszb9HnTbpxBfImVXeMz', max_length=64)),
            ('url_schema', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True)),
            ('appstore_url', self.gf('django.db.models.fields.URLField')(max_length=512, blank=True)),
        ))
        db.send_create_signal(u'v1', ['Application'])

        # Deleting field 'User.uuid'
        db.delete_column(u'v1_user', 'uuid')


    def backwards(self, orm):
        # Deleting model 'Device'
        db.delete_table(u'v1_device')

        # Deleting model 'Application'
        db.delete_table(u'v1_application')


        # User chose to not deal with backwards NULL issues for 'User.uuid'
        raise RuntimeError("Cannot reverse this migration. 'User.uuid' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'User.uuid'
        db.add_column(u'v1_user', 'uuid',
                      self.gf('django.db.models.fields.CharField')(max_length=255),
                      keep_default=False)


    models = {
        u'v1.answer': {
            'Meta': {'object_name': 'Answer'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'display_order': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_correct': ('django.db.models.fields.BooleanField', [], {}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.Question']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'v1.application': {
            'Meta': {'object_name': 'Application'},
            'appstore_url': ('django.db.models.fields.URLField', [], {'max_length': '512', 'blank': 'True'}),
            'client_key': ('django.db.models.fields.CharField', [], {'default': "'7h2CwPIjEeShTGD4Ha4KqA'", 'unique': 'True', 'max_length': '64'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'secret_key': ('django.db.models.fields.CharField', [], {'default': "u'3tiCK4BQWs9B8fm8y8Lb43cqOQr8xYqqm7lCzMdEqy7MdjTMRC6TTulxkGKr6fVL'", 'max_length': '64'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'url_schema': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        u'v1.device': {
            'Meta': {'object_name': 'Device'},
            'app_version': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'device_type': ('django.db.models.fields.SmallIntegerField', [], {'max_length': '1'}),
            'device_uuid': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_disabled_token': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'session_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'devices'", 'to': u"orm['v1.User']"})
        },
        u'v1.question': {
            'Meta': {'object_name': 'Question'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'end_at': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lottery_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '6'}),
            'start_at': ('django.db.models.fields.DateTimeField', [], {}),
            'thumbnail_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'time_limit': ('django.db.models.fields.IntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'winning_num': ('django.db.models.fields.IntegerField', [], {})
        },
        u'v1.quizanswerhistory': {
            'Meta': {'object_name': 'QuizAnswerHistory'},
            'answer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.Answer']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.Question']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.User']"})
        },
        u'v1.quizlotteryhistory': {
            'Meta': {'object_name': 'QuizLotteryHistory'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'itunes_code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.Question']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['v1.User']"})
        },
        u'v1.user': {
            'Meta': {'object_name': 'User'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['v1']