# -*- coding: utf-8 -*-

import os
import uuid
from v1 import utils

from django.utils.timezone import now
from django.db import models
from django.db.models.signals import post_save

class TimestampMixin(models.Model):
    created_at = models.DateTimeField(u'登録日時', auto_now_add=True)
    updated_at = models.DateTimeField(u'更新日時', auto_now=True)

    class Meta:
        abstract = True

class LogicalDeleteMixin(models.Model):
    is_deleted = models.BooleanField(u'削除フラグ', default=False)

    class Meta:
        abstract = True

    def logical_delete(self):
        self.is_deleted = True
        self.save()

def _generate_secret_key():
    import string
    from django.utils.crypto import get_random_string

    VALID_KEY_CHARS = string.ascii_letters + string.digits
    return get_random_string(64, VALID_KEY_CHARS)

class Application(TimestampMixin):
    name = models.CharField(max_length=45)
    client_key = models.CharField(max_length=64, default=utils.uuid_base64str, unique=True)
    secret_key = models.CharField(max_length=64, default=_generate_secret_key)
    url_schema = models.CharField(max_length=45, blank=True)
    appstore_url = models.URLField(max_length=512, blank=True)

    def __unicode__(self):
        return "%s: %s" % (self.id, self.name)

class User(TimestampMixin, LogicalDeleteMixin):
    def __unicode__(self):
        return "%s" % self.id

    class Meta:
        get_latest_by = 'created_at'

class Device(TimestampMixin):
    DEVICE_TYPE_CHOICES = (
        (1, 'ios'),
        (2, 'android'),
    )

    user = models.ForeignKey(User, related_name='devices')
    device_uuid = models.CharField(max_length=45)
    device_type = models.SmallIntegerField(choices=DEVICE_TYPE_CHOICES)
    session_id = models.CharField(max_length=40)
    app_version = models.CharField(max_length=32, blank=True, null=True)
    is_disabled_token = models.BooleanField(default=False)

    def __unicode__(self):
        return self.device_uuid

    class Meta:
        get_latest_by = 'created_at'

# -----------------------------------------------------------------------------
# question
# -----------------------------------------------------------------------------

def upload_thumbnail_to(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'v1/img/thumb/%s-%s%s' % (
        now().strftime("%Y%m%d%H%M%S"),
        str(uuid.uuid4()),
        filename_ext.lower(),
    )

class Question(TimestampMixin, LogicalDeleteMixin):
    thumbnail_image = models.ImageField(
        u'サムネイル画像',
        upload_to=upload_thumbnail_to,
        help_text=u'サイズ・形式'
    )
    content = models.CharField(u'問題内容', max_length=255)
    lottery_rate = models.DecimalField(
        u'当選確率',
        max_digits=9,
        decimal_places=6,
        help_text=u'0から100(%)までの数値(少数可能)を指定。0.1の場合は、1000回の抽選で1回当たる割合となる。'
    )
    time_limit = models.IntegerField(
        u'制限時間',
        help_text=u'半角数値・秒数',
    )
    winning_num = models.IntegerField(
        u'当選人数',
        help_text=u'半角数値',
    )
    start_at = models.DateTimeField(u'掲載開始日時')
    end_at = models.DateTimeField(u'掲載終了日時')

    class Meta:
        verbose_name = u'設問'
        verbose_name_plural = u'設問'

    def __unicode__(self):
        return "%s: %s" % (
                self.start_at, self.content[:50] +
            (u'...' if len(self.content) > 50 else '')
        )

    def on_create_question_count(self):
        QuizLotteryCount(question=self, num=0).save()


class Answer(TimestampMixin, LogicalDeleteMixin):
    question = models.ForeignKey(Question, verbose_name=u'設問')
    content = models.CharField(u'内容', max_length=255)
    display_order = models.IntegerField(u'表示順')
    is_correct = models.BooleanField(u'正解')

    class Meta:
        verbose_name = u'回答'
        verbose_name_plural = u'回答'

    def __unicode__(self):
        return "%s: %s" % (
            self.id, self.content[:50] +
            (u'...' if len(self.content) > 50 else '')
        )

class QuizLotteryHistory(TimestampMixin, LogicalDeleteMixin):
    user = models.ForeignKey(User, verbose_name=u'ユーザ')
    question = models.ForeignKey(Question, verbose_name=u'設問')
    itunes_code = models.CharField(u'iTunesコード', max_length=255)

    class Meta:
        verbose_name = u'当選ユーザ履歴'
        verbose_name_plural = u'当選ユーザ履歴'

    def __unicode__(self):
        return "%s" % (self.id)

class QuizLotteryCount(TimestampMixin, LogicalDeleteMixin):
    question = models.ForeignKey(Question, verbose_name=u'設問')
    num = models.IntegerField(u'当選人数合計', default=0)

    class Meta:
        verbose_name = u'当選人数'
        verbose_name_plural = u'当選人数'

    def __unicode__(self):
        return "%s" % (self.id)

class QuizAnswerHistory(TimestampMixin, LogicalDeleteMixin):
    user = models.ForeignKey(User, verbose_name=u'ユーザ')
    question = models.ForeignKey(Question, verbose_name=u'設問')
    answer = models.ForeignKey(Answer, verbose_name=u'回答')

    class Meta:
        verbose_name = u'回答履歴'
        verbose_name_plural = u'回答履歴'

    def __unicode__(self):
        return "%s" % (self.id)

class ItunesCode(TimestampMixin, LogicalDeleteMixin):
    code = models.CharField(u'iTunesコード', max_length=255)

    class Meta:
        verbose_name = u'itunesコード'
        verbose_name_plural = u'itunesコード'

    def __unicode__(self):
        return "%s: %s" % (self.id, self.code)

# -----------------------------------------------------------------------------
# hook
# -----------------------------------------------------------------------------
def question_post_save_hook(sender, instance, **kwargs):
    if kwargs['created']:
        instance.on_create_question_count()

post_save.connect(question_post_save_hook, sender=Question)
