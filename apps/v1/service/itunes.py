# -*- coding: utf-8 -*-

import traceback
from v1.models import ItunesCode

import logging
logger = logging.getLogger('v1')

def issue():
    try:
        itunes_code = ItunesCode.objects.select_for_update() \
            .filter(is_deleted=0).order_by('id')[0]
        itunes_code.is_deleted = 1
        itunes_code.save()
    except Exception, e:
        tb = traceback.format_exc()
        logger.error(e)
        logger.error(tb)
        return None
    return itunes_code.code
