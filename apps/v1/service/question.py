# -*- coding: utf-8 -*-

import random
from datetime import datetime
from django.conf import settings
from django.db import transaction

from v1.models import Question
from v1.exceptions import ValidationError
from v1.service import itunes as itunes_service

def _validate_get_questions(from_time):
    if from_time is None:
        raise ValidationError('from_time is requried')
    try:
        return datetime.strptime(from_time, '%Y%m%dT%H%M%S')
    except ValueError, ve:
        raise ValidationError(ve)

# アルファベット大文字小文字+数字(0-9, a-z, A-F)
def _gen_rand_str(length, chars=None):
    import random
    import string
    if chars is None:
        chars = string.digits + string.letters
        return ''.join([random.choice(chars) for i in range(length)])

@transaction.atomic
def lottery(question):
    """
    抽選処理
        グイズの当選率から抽選を行う
        当選した場合、次の処理を行う
        - 当選人数に達しているか確認
        - 当選枠があるのであれば、itunesコードを取得し、QuizLotteryCountテーブルの
          当選人数を更新する
    """
    weight = question.lottery_rate * 1000
    rand = random.uniform(1, 100000);
    status = True if weight >= rand else False
    code = None
    if (status):
        if (lottery_winning(question)):
            #code = _gen_rand_str(16)
            code = itunes_service.issue()
    data = {
        'question_id': question.id,
        'is_winner': True if (status and code) else False,
        'code':code,
    }
    return data

@transaction.atomic
def lottery_winning(question):
    """
    当選処理
    - 当選人数に達しているか確認
    - 当選枠があるのであれば、itunesコードを取得し、QuizLotteryCountテーブルの
      当選人数を更新する
    """
    quiz_count = question.quizlotterycount_set.select_for_update() \
        .filter(question=question)[0]
    if (question.winning_num > quiz_count.num):
        quiz_count.num = quiz_count.num + 1
        quiz_count.save()
        return True

    return False

def get_questions(from_time, to_time):
    """
    クイズの設問・回答を取得
    """
    start = _validate_get_questions(from_time)
    end = ''
    if to_time:
        end = _validate_get_questions(to_time)

    query_filter = {}
    if to_time:
        # 期間内に開始されたクイズを取得
        query_filter = {
            'start_at__gte': start,
            'start_at__lte': end,
        }
    else:
        # ある日のクイズを取得
        query_filter = {
            'start_at__lte': start,
            'end_at__gte': start,
        }

    query = Question.objects.select_related('answer') \
            .values(
                'id', 'thumbnail_image', 'content', 'lottery_rate', 'time_limit',
                'winning_num', 'start_at', 'end_at',
                'answer__id', 'answer__question_id', 'answer__content',
                'answer__display_order', 'answer__is_correct',
            ) \
            .filter(**query_filter) \
            .order_by('id', 'answer__id')

    question_list = list(query)
    if (len(question_list) == 0):
        return {
            'questions': [],
            'count': 0,
        }

    first_data = question_list.pop(0)
    questions = []
    answers = []

    q_id = first_data['id']
    thumbnail_image = 'http://{0}{1}{2}'.format(
        settings.MEDIA_HOST_NAME,
        settings.MEDIA_URL,
        first_data['thumbnail_image']
    )
    content = first_data['content']
    lottery_rate = first_data['lottery_rate']
    time_limit = first_data['time_limit']
    winning_num = first_data['winning_num']
    start_at = first_data['start_at']
    end_at = first_data['end_at']

    answers.append({
        'id': first_data['answer__id'],
        'question_id': first_data['answer__question_id'],
        'content': first_data['answer__content'],
        'display_order': first_data['answer__display_order'],
        'is_correct': first_data['answer__is_correct'],
    })

    for data in question_list:
        if q_id == data['id']:
            answers.append({
                'id': data['answer__id'],
                'question_id': data['answer__question_id'],
                'content': data['answer__content'],
                'display_order': data['answer__display_order'],
                'is_correct': data['answer__is_correct'],
            })
        else:
            questions.append({
                'id': q_id,
                'thumbnail_image': thumbnail_image,
                'content': content,
                'lottery_rate': lottery_rate,
                'time_limit': time_limit,
                'winning_num': winning_num,
                'start_at': start_at,
                'end_at': end_at,
                'answers': {
                    'paterns': answers,
                    'count': len(answers)
                }
            })
            #初期化
            q_id = data['id']
            thumbnail_image = 'http://{0}{1}{2}'.format(
                settings.MEDIA_HOST_NAME,
                settings.MEDIA_URL,
                first_data['thumbnail_image']
            )
            content = data['content']
            lottery_rate = data['lottery_rate']
            time_limit = data['time_limit']
            winning_num = data['winning_num']
            start_at = data['start_at']
            end_at = data['end_at']

            answers = []
            answers.append({
                'id': data['answer__id'],
                'question_id': data['answer__question_id'],
                'content': data['answer__content'],
                'display_order': data['answer__display_order'],
                'is_correct': data['answer__is_correct'],
            })

    if len(answers) > 0:
        questions.append({
            'id': q_id,
            'thumbnail_image': thumbnail_image,
            'content': content,
            'lottery_rate': lottery_rate,
            'time_limit': time_limit,
            'winning_num': winning_num,
            'start_at': start_at,
            'end_at': end_at,
            'answers': {
                'paterns': answers,
                'count': len(answers)
            }
        })

    return {
        'questions': questions,
        'count': len(questions),
    }
