# -*- coding:utf-8 -*-

import hashlib
import logging
import string

from django import forms
from django.conf import settings
from django.db import transaction
from django.db.models import Q
from django.utils.crypto import get_random_string

from v1 import utils
from v1.models import (
Application,
Device,
User,
)

logger = logging.getLogger('v1')

class RegisterUserForm(forms.Form):
    """
    register_user用の入力値検証フォーム
    """
    device_uuid = forms.CharField(max_length=45)
    device_type = forms.CharField(max_length=10)
    client_key = forms.CharField(max_length=64)
    digest = forms.CharField(max_length=100)
    ua = forms.CharField(required=False)

    def clean(self):
        """
        client_keyがDBに存在する値かチェックする
        Digestの値が正しいかチェック
        """
        if len(self.errors.keys()) == 0:
            client_key = self.cleaned_data['client_key']
            try:
                requested_app = Application.objects.get(client_key=client_key)
            except Application.DoesNotExist:
                raise forms.ValidationError('Invalid client key')

            if self.cleaned_data['digest'] != hashlib.sha1(
                    self.cleaned_data['client_key'] + ':' +
                    requested_app.secret_key + ':' +
                    self.cleaned_data['device_uuid']).hexdigest():
                raise forms.ValidationError('Invalid digest')
        return self.cleaned_data

def register_user(params):
    """
    ユーザー登録処理

    既に存在するdevice_uuidの場合は既存のセッションを返す
    """
    f = RegisterUserForm(params)
    if not f.is_valid():
        logger.debug(utils.get_form_errors(f))
        raise forms.ValidationError(utils.get_form_errors(f))

    try:
        device = Device.objects.get(device_uuid=params['device_uuid'])
        user = device.user
    except Device.DoesNotExist:
        user, device = create_user(**f.cleaned_data)

    return {
        'user_id': user.id,
        'session_id': device.session_id,
    }

@transaction.atomic
def create_user(device_uuid, ua=None, **kwargs):
    """
    ユーザー作成処理、次のレコードを作成する
    - User
    - Device
    """
    user = User()
    user.save()

    VALID_KEY_CHARS = string.ascii_lowercase + string.digits
    device = Device(
        user=user,
        device_uuid=device_uuid,
        device_type=1,  # iOS only
        #app_version=utils.extract_app_version_from_ua(ua),
        session_id=get_random_string(40, VALID_KEY_CHARS),
    )
    device.save()
    return (user, device)
