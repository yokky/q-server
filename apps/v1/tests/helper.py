# -*- coding: utf-8 -*-

import json
import hashlib
import urllib

from django.conf import settings
import django.test.client

from v1.models import Application
#from v1.utils import validate_by_json_schema

DUMMY1_SESSION = 'XXX'
DUMMY1_USER_ID = 1

DUMMY_SESSION = 'luxff3gc6ub1f0zcggkp5u8dtugb0vpuc0lmj5i2'
DUMMY_USER_ID = 2

def get_result(response):
    body = json.loads(response.content, encoding='utf-8')
    return body.get('result')


def get_error(response):
    body = json.loads(response.content, encoding='utf-8')
    return body.get('error')


def get_warning(response):
    body = json.loads(response.content, encoding='utf-8')
    return body.get('warning')


#def assert_error_code(response, code, msg=None):
#    body = json.loads(response.content, encoding='utf-8')
#    try:
#        eq_(body.get('error').get('code'), code, msg)
#    except AttributeError:
#        eq_(None, code, msg)
#
#
#def assert_no_result(response, msg=None):
#    body = json.loads(response.content, encoding='utf-8')
#    eq_(body.get('result'), None, msg)
#
#
#def assert_result(response, content=None, msg=None):
#    body = json.loads(response.content, encoding='utf-8')
#    ok_(body.get('result'), "Result property exist")
#    if content:
#        eq_(body.get('result'), content, msg)
#
#
#def assert_no_error(response):
#    body = json.loads(response.content, encoding='utf-8')
#    eq_(body.get('error'), None)
#
#
#def assert_error(response):
#    body = json.loads(response.content, encoding='utf-8')
#    ok_(body.get('error'))
#

def get_error_msg(response):
    body = json.loads(response.content, encoding='utf-8')
    try:
        return body.get('error').get('message')
    except AttributeError:
        return None


def create_digest(client_key, uuid):
    app = Application.objects.get(client_key=client_key)
    return hashlib.sha1(
        str(client_key) + ":" +
        app.secret_key + ":" +
        uuid).hexdigest()


def formdata(param_dict):
    return urllib.urlencode(param_dict)


def validate_response_with_schema(response, schema_name, link_index):
    return true
    #obj = json.loads(response.content, encoding='utf-8')
    #return validate_by_json_schema(obj, schema_name, link_index, 'targetSchema')


#class Client(django.test.client.Client):
#    def post(self, path, data={}, content_type='application/json', follow=False, **extra):
#        if content_type == 'application/json':
#            data = json.dumps(data)
#        return super(Client, self).post(path, data=data, content_type=content_type, **extra)
#
#    def put(self, path, data='', content_type='application/json', follow=False, **extra):
#        if content_type == 'application/json':
#            data = json.dumps(data)
#        return super(Client, self).put(path, data=data, content_type=content_type, **extra)
