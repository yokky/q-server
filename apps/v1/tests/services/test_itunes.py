# -*- coding: utf-8 -*-

from django.db import transaction
from django.test import TestCase, TransactionTestCase

from v1.models import (
ItunesCode,
)
from v1.service import (
itunes as itunes_service,
)

class ItunesCodeTest(TransactionTestCase):
    """
    Itunesコードの発行テスト
    """
    fixtures = ['fixtures.json']

    def setUp(self):
        self.itunes_code = ItunesCode.objects.get(pk=1)
        self.itunes_code_2 = ItunesCode.objects.get(pk=2)

    @transaction.atomic
    def test_issue_success(self):
        """ 通常の発行 """
        code = itunes_service.issue()
        self.assertIsNotNone(code)
        self.assertEqual(code, self.itunes_code.code)

    @transaction.atomic
    def test_issue_success_2(self):
        """ 連続してコード発行を行う """
        code = itunes_service.issue()
        self.assertIsNotNone(code)
        self.assertEqual(code, self.itunes_code.code)

        code = itunes_service.issue()
        self.assertIsNotNone(code)
        self.assertEqual(code, self.itunes_code_2.code)

        code = itunes_service.issue()
        self.assertIsNone(code)
