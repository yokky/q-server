# -*- coding: utf-8 -*-

from datetime import datetime
from django.test import TestCase, TransactionTestCase

from v1.models import (
ItunesCode,
Question,
QuizLotteryCount,
)
from v1.service import (
question as question_service,
)

class QuizLotteryTest(TestCase):
    """
    抽選処理のテスト
    """
    fixtures = ['fixtures.json']

    def setUp(self):
        self.question = Question.objects.get(pk=3)
        self.quiz_count = self.question.quizlotterycount_set \
            .filter(question=self.question)[0]

    def test_lottery_success(self):
        self.question.lottery_rate = 100
        data = question_service.lottery(self.question)

        self.assertEqual(data['question_id'], self.question.id)
        self.assertEqual(data['is_winner'], True)
        self.assertIsNotNone(data['code'])

        quiz_count = self.question.quizlotterycount_set \
            .filter(question=self.question)[0]
        self.assertEqual(quiz_count.id, self.quiz_count.id)
        self.assertEqual(quiz_count.num, self.quiz_count.num + 1)

        # 当選人数に達しているので、ハズレになるか確認
        data2 = question_service.lottery(self.question)

        self.assertEqual(data2['question_id'], self.question.id)
        self.assertEqual(data2['is_winner'], False)
        self.assertIsNone(data2['code'])

        quiz_count = self.question.quizlotterycount_set \
            .filter(question=self.question)[0]
        self.assertEqual(quiz_count.id, self.quiz_count.id)
        self.assertEqual(quiz_count.num, self.quiz_count.num + 1)

    def test_lottery_success(self):
        # Itunesコードの在庫がなく、当選した場合
        ItunesCode.objects.all().delete()

        self.question.lottery_rate = 100
        data = question_service.lottery(self.question)

        self.assertEqual(data['question_id'], self.question.id)
        self.assertEqual(data['is_winner'], False)

        self.assertIsNone(data['code'])

        quiz_count = self.question.quizlotterycount_set \
            .filter(question=self.question)[0]
        self.assertEqual(quiz_count.id, self.quiz_count.id)
        self.assertEqual(quiz_count.num, self.quiz_count.num + 1)

    def test_lottery_fail(self):
        self.question.lottery_rate = 0
        data = question_service.lottery(self.question)

        self.assertEqual(data['question_id'], self.question.id)
        self.assertEqual(data['is_winner'], False)
        self.assertIsNone(data['code'])

        quiz_count = self.question.quizlotterycount_set \
            .filter(question=self.question)[0]
        self.assertEqual(quiz_count.id, self.quiz_count.id)
        self.assertEqual(quiz_count.num, self.quiz_count.num)

class QuizLotteryWinningTest(TransactionTestCase):
    """
    抽選処理のテスト
    """
    fixtures = ['fixtures.json']

    def setUp(self):
        self.question = Question.objects.get(pk=3)
        self.quiz_count = self.question.quizlotterycount_set \
            .filter(question=self.question)[0]

    def test_success(self):
        question_service.lottery_winning(self.question)
        quiz_count = self.question.quizlotterycount_set \
            .filter(question=self.question)[0]
        self.assertEqual(quiz_count.id, self.quiz_count.id)
        self.assertEqual(quiz_count.num, self.quiz_count.num + 1)

    def test_fail(self):
        """当選人数に達した場合"""
        self.question.winning_num = 0
        self.question.save()

        question_service.lottery_winning(self.question)
        quiz_count = self.question.quizlotterycount_set \
            .filter(question=self.question)[0]
        self.assertEqual(quiz_count.id, self.quiz_count.id)
        self.assertEqual(quiz_count.num, self.quiz_count.num)
