# -*- coding: utf-8 -*-

from django.test.client import Client
from django.core.management import call_command
from django.test import TestCase

from v1.service import user
from v1.models import User, Device
from v1.tests.helper import *

class QuestionAPINotAllowedMethodTest(TestCase):

    fixtures = ['fixtures.json']

    def test_delete(self):
        """
        DELETE /questions not allowd
        """
        response = Client().delete('/v1/questions', {
            }, HTTP_X_Q_SESSION=DUMMY_SESSION)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 403)

    def test_put(self):
        """
        PUT /questions not allowd
        """
        response = Client().put('/v1/questions', {
            }, HTTP_X_Q_SESSION=DUMMY_SESSION)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 403)

    def test_get(self):
        """
        GET /questions not allowd
        """
        response = Client().get('/v1/questions', {
            'from_time': 'a'
            }, HTTP_X_Q_SESSION=DUMMY_SESSION)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 403)

    def test_post(self):
        """
        POST /questions not allowd
        """
        response = Client().post('/v1/questions', {
            'from_time': 'b'
            }, HTTP_X_Q_SESSION=DUMMY_SESSION)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 403)

    def test_get_success(self):
        """
        GET /questions allowd
        ある１日の出題クイズを取得
        """
        device = Device.objects.get(pk=1)
        response = Client().get('/v1/questions', {
            'from_time': '20150505T000000'
            }, HTTP_X_Q_SESSION=device.session_id)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)

    def test_get_success_period_ok(self):
        """
        GET /questions allowd
        期間指定にてクイズを取得(ok)
        """
        device = Device.objects.get(pk=1)
        response = Client().get('/v1/questions', {
            'from_time': '20150504T000000',
            'to_time':   '20150830T000000'
            }, HTTP_X_Q_SESSION=device.session_id)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)

    def test_get_success_period_ng(self):
        """
        GET /questions allowd
        期間指定にてクイズを取得(ng)
        """
        device = Device.objects.get(pk=1)
        response = Client().get('/v1/questions', {
            'from_time': '20150830T000000',
            'to_time':   '20150504T000000'
            }, HTTP_X_Q_SESSION=device.session_id)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
