# -*- coding: utf-8 -*-

from django.test.client import Client
from django.core.management import call_command
from django.test import TestCase


from v1.service import user
from v1.models import User, Device
from v1.tests.helper import *

class UserAPINotAllowedMethodTest(TestCase):
    def test_delete(self):
        """
        DELETE /users not allowd
        """
        response = Client().delete('/v1/users', {
            }, HTTP_X_Q_SESSION=DUMMY_SESSION)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 405)

    def test_put(self):
        """
        PUT /users not allowd
        """
        response = Client().put('/v1/users', {
            }, HTTP_X_Q_SESSION=DUMMY_SESSION)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 405)

class UserAPICreateTest(TestCase):
    def setUp(self):
        self.before_user_num = User.objects.count()
        self.before_device_num = Device.objects.count()
        self.application = Application(name='test1')
        self.application2 = Application(name='test2')
        self.application.save()
        self.application2.save()

    def test_invalid_params(self):
        """
        POST /users Invalid param cause validation error
        """
        response = Client().post('/v1/users', {
            "xxxx": "99999",
            })
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 400)
        #assert_error_code(response, 'VALIDATION_ERROR')

        self.assertEqual(User.objects.count(), self.before_user_num, 'Failed to add user')
        self.assertEqual(Device.objects.count(), self.before_device_num, 'Failed to add device')

    def test_nomal_case(self):
        """
        POST /users Normal case
        """
        response = Client().post('/v1/users', {
            'device_uuid': 'xxxxx-yyyyy-zzzzz-normal',
            'device_type': 'ios',
            'client_key': self.application.client_key,
            'digest': create_digest(self.application.client_key, 'xxxxx-yyyyy-zzzzz-normal')
        }, HTTP_USER_AGENT='quize/1.0.0')
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
        #assert_no_error(response)
        self.assertEqual(User.objects.count(), self.before_user_num + 1, 'Succeeded to add user')
        self.assertEqual(Device.objects.count(), self.before_device_num + 1, 'Succeed to add device')

        self.assertIsNotNone(get_result(response).get('user_id'))

        result = get_result(response)
        self.assertEqual(type(result.get('user_id')), int)
        self.assertEqual(type(result.get('session_id')), unicode)

        # Check session
        session_id = get_result(response).get('session_id')
        device = Device.objects.get(session_id=session_id)
        self.assertEqual(device.device_uuid, 'xxxxx-yyyyy-zzzzz-normal')

    def test_without_digest(self):
        """
        POST /users without digest
        """
        response = Client().post('/v1/users', {
            "device_uuid": "xxxxx-yyyyy-zzzzz-normal",
            "device_type": "ios",
            "device_token": "test_device_token",
            'client_key': self.application.client_key,
            })
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 400)
        #assert_error(response)
        #assert_error_code(response, 'VALIDATION_ERROR')
        self.assertEqual(User.objects.count(), self.before_user_num, 'Failed to add user')
        self.assertEqual(Device.objects.count(), self.before_device_num, 'Failed to add device')

    def test_invalid_digest(self):
        """
        POST /users without digest
        """
        response = Client().post('/v1/users', {
            "device_uuid": "xxxxx-yyyyy-zzzzz-normal",
            "device_type": "ios",
            "device_token": "test_device_token",
            'client_key': self.application.client_key,
            "digest": "aaaaaaaaa"
            })
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 400)
        #assert_error(response)
        #assert_error_code(response, 'VALIDATION_ERROR')
        self.assertEqual(User.objects.count(), self.before_user_num, 'Failed to add user')
        self.assertEqual(Device.objects.count(), self.before_device_num, 'Failed to add device')

    def test_save_device(self):
        """
        POST /users with already exist device_uuid
        """
        params = {
            "device_uuid": "xxxxx-yyyyy-zzzzz5",
            "device_type": "ios",
            'client_key': self.application2.client_key,
            'digest': create_digest(self.application2.client_key, 'xxxxx-yyyyy-zzzzz5')
        }
        response1 = Client().post('/v1/users', params)
        self.assertEqual(response1.status_code, 200)
        self.assertIsNotNone(response1)
        #assert_no_error(response1)
        session_id1 = get_result(response1).get('session_id')

        response2 = Client().post('/v1/users', params)
        self.assertIsNotNone(response2)
        #assert_no_error(response1)
        session_id2 = get_result(response2).get('session_id')

        self.assertEqual(session_id1, session_id2)
        self.assertEqual(User.objects.count(), self.before_user_num + 1, 'Only 1 user added')
        self.assertEqual(Device.objects.count(), self.before_device_num + 1, 'Only 1 device added')
