# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from .views import (
indexes,
)

urlpatterns = patterns('',
    url('questions$', indexes.question_list, name='questions'),
    url('lottery$', indexes.lottery, name='lottery'),
    url('users$', indexes.users, name='users'),
)
