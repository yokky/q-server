# -*- coding: utf-8 -*-

import logging
import uuid

from django.conf import settings
from django.utils.safestring import mark_safe
from django.utils.text import normalize_newlines

logger = logging.getLogger('v1')

def uuid_base64str():
    return uuid.uuid1().bytes.encode('base64').rstrip('=\n').replace('/', '_')

def get_form_errors(f):
    return [(k, f.error_class.as_text(v)) for k, v in f.errors.items()]

# log filter
class GeneralLogFilter(logging.Filter):
    def filter(self, record):
        return record.levelno < logging.WARNING

def normalize_text(value):
    """
    Removes all newline characters from a block of text.
    """
    if value is None or len(value) <= 0:
        return value
    # First normalize the newlines using Django's nifty utility
    normalized_text = normalize_newlines(value)
    # Then simply remove the newlines like so.
    text = mark_safe(normalized_text.replace('\n', ''))
    return text

def escape_newline(value):
    if value is None or len(value) <= 0:
        return value
    normalized_text = normalize_newlines(value)
    return normalized_text.replace('\n', '\\n')

def unescape_newline(value):
    if value is None or len(value) <= 0:
        return value
    return value.replace('\\n', '\n')

def nl2br(value):
    if value is None or len(value) <= 0:
        return value
    normalized_text = normalize_newlines(value)
    return normalized_text.replace('\n', '<br />')
