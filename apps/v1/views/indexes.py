# -*- coding: utf-8 -*-

import json
import decimal
import random
from datetime import datetime

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.forms import ValidationError

from v1.service import (
question as question_service,
user as user_service,
)
from v1.models import Question, Device
from v1.exceptions import (
QServerError,
PermissionError,
)

def support_datetime_default(o):
    if isinstance(o, datetime):
        return o.isoformat()
    if isinstance(o, decimal.Decimal):
        return float(o)
    raise TypeError(repr(o) + " is not JSON serializable")

def render_json_response(request, data, status=None):
    '''response を JSON で返却'''
    json_str = json.dumps(
        data,
        ensure_ascii=False,
        indent=2,
        default=support_datetime_default,
    )
    callback = request.GET.get('callback')
    if not callback:
        callback = request.REQUEST.get('callback')  # POSTでJSONPの場合
    if callback:
        json_str = "%s(%s)" % (callback, json_str)
        response = HttpResponse(json_str, content_type='application/javascript; charset=UTF-8', status=status)
    else:
        response = HttpResponse(json_str, content_type='application/json; charset=UTF-8', status=status)
    return response

def _auth_check(session_id):
    device = Device.objects.filter(session_id=session_id)
    device = list(device)
    if (device):
        return True
    return False

@csrf_exempt
def users(request):
    """
    ユーザ登録を行う
    """
    result = {}
    result_body = None
    error = None
    status = None;
    if (request.method == 'POST'):
        try:
            params = request.POST.copy()
            params['ua'] = request.META.get('HTTP_USER_AGENT')
            result_body = user_service.register_user(params)
        except ValidationError as e:
            error = unicode(e)
            status = 400;
    else:
        error = '-'
        status = 405;
    result = {
        'result': result_body,
        'error' : error
    }
    return render_json_response(request, result, status)

def question_list(request):
    """
    クイズの設問と回答のJSONを生成
    """
    from_time = request.GET.get('from_time')
    to_time = request.GET.get('to_time')

    if not from_time:
        now_datetime = datetime.now()
        from_time = now_datetime.strftime('%Y%m%dT%H%M%S')
    # クイズの登録運用コスト削減のため
    # 過去の設問を使い回す
    from_time = datetime.now().strftime('201510%dT000000')

    status = None
    try:
        session_id = request.META.get('HTTP_X_Q_SESSION')
        if (_auth_check(session_id) == False):
            raise PermissionError(message='')
        questions = question_service.get_questions(from_time, to_time)
        result = {
            'result': questions,
            'error' : None
        }
    except PermissionError as e:
        result = {
            'result': None,
            'error' : e.code
        }
        status = e.http_status
    except QServerError as e:
        result = {
            'result': None,
            'error' : unicode(e)
        }
    return render_json_response(request, result, status)

@csrf_exempt
def lottery(request):
    """
    抽選を行う
    """
    result = {}
    result_body = None
    error = None
    status = None;
    if (request.method == 'POST'):
        try:
            session_id = request.META.get('HTTP_X_Q_SESSION')
            if (_auth_check(session_id) == False):
                raise PermissionError(message='')
            question_id = request.POST.get('question_id')
            question = Question.objects.get(pk=question_id)
            result_body = question_service.lottery(question)
        # .. todo: 例外処理を適切にする
        except PermissionError as e:
            error = e.code
            status = e.http_status
        #except Exception as e:
        #    error = unicode(e)
        #    status = 400
    else:
        error = '-'
        status = 405;
    result = {
        'result': result_body,
        'error': error
    }
    return render_json_response(request, result, status)
