# -*- coding: utf-8 -*-

"""
Quize デプロイスクリプト

使い方:
    fab -R [production] [コマンド]

コマンド:
    deploy                 # デプロイを実施する
    deploy:reset=          # git reset --hard を無効にする
    rollback:[commits]     # commits は HEAD から戻りたいコミット数
    status                 # アプリの状態を表示する
    restart                # アプリを再起動する
    uptime                 # サーバの uptime を表示する
"""

import time
from fabric.api import env, run, local, cd
from fabric.colors import red

env.roledefs = {
    'production': ['tk2-212-15971.vs.sakura.ne.jp']
}
env.user = 't-yokoyama'
env.port = 22

path = '/web/apps/q-server/'
init_front = path + 'config/init.d/q-server-front'
init_staff = path + 'config/init.d/q-server-staff'

def _get_git(roles):
    if 'staging' in roles:
        return local
    return run

def uptime():
    run("uptime")

def status():
    run("%s status" % init_front)
    run("%s status" % init_staff)

def restart():
    run("%s restart" % init_front)
    run("%s restart" % init_staff)

def rollback(commits=1):
    git = _get_git(env.roles)
    with cd(path):
        timestamp = time.strftime('%y%m%d%H%M%S')
        git("git reset --hard")
        git("git checkout -b rollback/%s HEAD~%d" % (timestamp, int(commits)))
        restart()

def deploy(reset=True):
    git = _get_git(env.roles)
    with cd(path):
        if bool(reset):
            git("git reset --hard")
        git("git checkout master")
        git("git pull origin master")
        restart()
