#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import os
import sys
#import vbundler
#import newrelic.agent

# apps を sys.path に追加
apps_path = os.path.join(sys.path[0], 'apps')
sys.path.insert(0, apps_path)

if __name__ == "__main__":
    #vbundler.Commands.inspect('./PyFile')
    #newrelic.agent.initialize('./newrelic.ini')
    #os.environ.setdefault("DJANGO_SETTINGS_MODULE", "q_server.settings.front_local")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
