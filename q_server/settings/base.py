# -*- coding: utf-8 -*-

#==============================================================================
# プロジェクト共通の設定を行う。
# プロジェクトの信頼性を保つ同時に環境に対する情報共有をスムーズ行うため
# すべての設定はこの base.py から派生する必要がある。
#==============================================================================

DEBUG = False
SHOP_DEBUG = False

SITE_ID = 1

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.6/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

#==============================================================================
# タイムゾーンの設定
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name または /usr/share/zoneinfo を参照
#==============================================================================
TIME_ZONE = 'Asia/Tokyo'
USE_TZ = False
USE_I18N = True
USE_L10N = True
LANGUAGE_CODE = 'ja'
LANGUAGES = (
    ('ja', 'Japanese'),
    ('en', 'English'),
)
