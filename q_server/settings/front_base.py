# -*- coding: utf-8 -*-
from .base import *

#==============================================================================
# フロントアプリの共通設定を行う。
#==============================================================================

import os
import inspect
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS, MIDDLEWARE_CLASSES, AUTHENTICATION_BACKENDS

# Make this unique, and don't share it with anybody.
SECRET_KEY = '(cxnugg82b@sxbxkt*$!fvj+j$)g(#=r+)77q90-!vybv0x=#n'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    #'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
#    'south',
    'v1',
#    'demo',
    'social_auth',
)

PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '../../'))

#==============================================================================
# URL
#==============================================================================

ROOT_URLCONF = ''

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(PROJECT_DIR, './static')
MEDIA_ROOT = os.path.join(PROJECT_DIR, './media')

STATICFILES_DIRS = (
#    os.path.join(PROJECT_DIR, 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

#==============================================================================
# Templates
#==============================================================================

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, './templates'),
    os.path.join(PROJECT_DIR, './apps/c1/templates'),
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    'django.core.context_processors.request',
    'social_auth.context_processors.social_auth_by_name_backends',
)


#==============================================================================
# Middleware
#==============================================================================

MIDDLEWARE_CLASSES += (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
#    'django.middleware.transaction.TransactionMiddleware',
)

#==============================================================================
# Auth / security
#==============================================================================

AUTHENTICATION_BACKENDS += (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
#    'social_auth.backends.google.GoogleOAuthBackend',
#    'social_auth.backends.google.GoogleOAuth2Backend',
#    'social_auth.backends.google.GoogleBackend',
#    'social_auth.backends.yahoo.YahooBackend',
#    'social_auth.backends.browserid.BrowserIDBackend',
#    'social_auth.backends.contrib.linkedin.LinkedinBackend',
#    'social_auth.backends.contrib.disqus.DisqusBackend',
#    'social_auth.backends.contrib.livejournal.LiveJournalBackend',
#    'social_auth.backends.contrib.orkut.OrkutBackend',
#    'social_auth.backends.contrib.foursquare.FoursquareBackend',
#    'social_auth.backends.contrib.github.GithubBackend',
#    'social_auth.backends.contrib.vk.VKOAuth2Backend',
#    'social_auth.backends.contrib.live.LiveBackend',
#    'social_auth.backends.contrib.skyrock.SkyrockBackend',
#    'social_auth.backends.contrib.yahoo.YahooOAuthBackend',
#    'social_auth.backends.contrib.readability.ReadabilityBackend',
#    'social_auth.backends.contrib.fedora.FedoraBackend',
#    'social_auth.backends.OpenIDBackend',
    'django.contrib.auth.backends.ModelBackend',
)

#==============================================================================
# Socila Auth Backends
#==============================================================================
# 必要なソーシャルサービスに応じて認証に必要な情報を記述
TWITTER_CONSUMER_KEY         = 'your consumer key'
TWITTER_CONSUMER_SECRET      = 'your cosumer secret'
FACEBOOK_APP_ID              = '238384626499125'
FACEBOOK_API_SECRET          = 'c194f27b512d0fd4e87b24d9d0918fcf'
FACEBOOK_EXTENDED_PERMISSIONS = ['email']
 
# ログインを行うURLなどの設定
LOGIN_URL          = '/login/'
#LOGIN_REDIRECT_URL = '/dashboard/'
LOGIN_REDIRECT_URL = '/c1/modori'
LOGIN_ERROR_URL    = '/login-error/'

# これがないと、シリアライズエラーとなる1
SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'

#==============================================================================
# Logging
#==============================================================================

q_server_env = \
    os.environ['Q_SERVER_ENV'] if 'Q_SERVER_ENV' in os.environ else None

LOG_FILE_PATH = None
ERROR_LOG_FILE_PATH = None
if (q_server_env == 'staging') or (q_server_env == 'production'):
    LOG_FILE_PATH = '/var/log/app/q_server_front.log'
    ERROR_LOG_FILE_PATH = '/var/log/app/q_server_front.err.log'
else:
    LOG_FILE_PATH = '/tmp/q_server_front.log'
    ERROR_LOG_FILE_PATH = '/tmp/q_server_front.err.log'

handler = 'console' if LOG_FILE_PATH is None else 'file'
error_handler = 'console' if ERROR_LOG_FILE_PATH is None else 'error_file'

logger = lambda level: {
    'level': level,
    'handlers': list(set([handler, error_handler])),
    'propagate': True,
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'general_log': {
            '()': 'q_v1.utils.GeneralLogFilter',
        },
    },
    'formatters': {
        'normal': {
            'format': '%(asctime)s pid:%(process)d %(levelname)s %(module)s: %(message)s',
        }
    },
    'handlers': (lambda: {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'normal',
        }
    } if LOG_FILE_PATH is None else {
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': LOG_FILE_PATH,
            'formatter': 'normal',
            'filters': ['general_log'],
        },
        'error_file': {
            'level': 'WARN',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': ERROR_LOG_FILE_PATH or LOG_FILE_PATH,
            'formatter': 'normal',
            'filters': ['require_debug_false'],
        },
    })(),
    'loggers': {
        'django.request': logger('WARNING'),
        'django.db.backends': logger('DEBUG'),
        'v1': logger('DEBUG'),
        'demo': logger('DEBUG'),
    }
}

#==============================================================================
# Email
#==============================================================================
#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
#EMAIL_HOST = ''
#EMAIL_PORT =25
