# -*- coding: utf-8 -*-
from q_server.settings.front_base import *

DEBUG = False
SHOP_DEBUG = False

ALLOWED_HOSTS = [
    'tk2-212-15971.vs.sakura.ne.jp'
]

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     'q',
        'USER':     'root',
        'PASSWORD': 'yokohide',
        'HOST':     '127.0.0.1',
        'PORT':     '3306',
        #'OPTIONS' : {'init_command':'SET storage_engine=INNODB'},
    },
}

MIDDLEWARE_CLASSES += (
)

ROOT_URLCONF = 'q_server.urls.front_production'

INTERNAL_IPS = ('127.0.0.1',)

INSTALLED_APPS += (
)

MEDIA_HOST_NAME = 'tk2-212-15971.vs.sakura.ne.jp'

#==============================================================================
# FastCGI
#==============================================================================
FORCE_SCRIPT_NAME = ''
