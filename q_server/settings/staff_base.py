# -*- coding: utf-8 -*-
from .base import *

#==============================================================================
# 管理画面の共通設定を行う。
#==============================================================================

import os
import inspect
import sys
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS, MIDDLEWARE_CLASSES, AUTHENTICATION_BACKENDS

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'ye#3+*it^mwh$yci@#+wl_n5)&#rdh)+ps5z&v_xa5pd(6b)9o'

INSTALLED_APPS = (
#    'demo',
    'v1',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    #'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
#    'south',
)

PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '../../'))

#==============================================================================
# URL
#==============================================================================

ROOT_URLCONF = ''

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(PROJECT_DIR, './static')
MEDIA_ROOT = os.path.join(PROJECT_DIR, './media')

STATICFILES_DIRS = (
#    os.path.join(PROJECT_DIR, 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

#==============================================================================
# Templates
#==============================================================================

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, './templates'),
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    'django.core.context_processors.request',
)


#==============================================================================
# Middleware
#==============================================================================

MIDDLEWARE_CLASSES += (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.middleware.transaction.TransactionMiddleware',
)

#==============================================================================
# Auth / security
#==============================================================================

AUTHENTICATION_BACKENDS += (
)

#==============================================================================
# Logging
#==============================================================================

q_server_env = \
    os.environ['Q_SERVER_ENV'] if 'Q_SERVER_ENV' in os.environ else None

LOG_FILE_PATH = None
ERROR_LOG_FILE_PATH = None
if (q_server_env == 'staging') or (q_server_env == 'production'):
    LOG_FILE_PATH = '/var/log/app/q_v1_staff.log'
    ERROR_LOG_FILE_PATH = '/var/log/app/q_v1_staff.err.log'
else:
    LOG_FILE_PATH = '/tmp/q_v1_staff.log'
    ERROR_LOG_FILE_PATH = '/tmp/q_v1_staff.err.log'

handler = 'console' if LOG_FILE_PATH is None else 'file'
error_handler = 'console' if ERROR_LOG_FILE_PATH is None else 'error_file'

logger = lambda level: {
    'level': level,
    #'handlers': list(set([handler, error_handler, 'mail_admins'])),
    'handlers': list(set([handler, error_handler])),
    'propagate': True,
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'general_log': {
            '()': 'q_v1.utils.GeneralLogFilter',
        },
    },
    'formatters': {
        'normal': {
            'format': '%(levelname)s %(asctime)s %(module)s: %(message)s',
        }
    },
    'handlers': (lambda: {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'normal',
        },
    } if LOG_FILE_PATH is None else {
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': LOG_FILE_PATH,
            'when': 'midnight',
            'formatter': 'normal',
            'filters': ['general_log'],
        },
        'error_file': {
            'level': 'WARN',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': ERROR_LOG_FILE_PATH or LOG_FILE_PATH,
            'when': 'midnight',
            'formatter': 'normal',
            'filters': ['require_debug_false'],
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
        },
    })(),
    'loggers': {
        'django.request': logger('WARNING'),
        'django.db.backends': logger('DEBUG'),
        'q_v1': logger('DEBUG'),
    }
}

#==============================================================================
# Email
#==============================================================================
# エラーメール受信用
#ADMINS = ()
#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST = "localhost"
EMAIL_PORT =25
