# -*- coding: utf-8 -*-
from q_server.settings.staff_base import *

DEBUG = False
SHOP_DEBUG = False

ALLOWED_HOSTS = [
    'staff.tk2-212-15971.vs.sakura.ne.jp'
]

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     'q',
        'USER':     'root',
        'PASSWORD': 'yokohide',
        'HOST':     '127.0.0.1',
        'PORT':     '3306',
        #'OPTIONS' : {'init_command':'SET storage_engine=INNODB'},
    },
}

MIDDLEWARE_CLASSES += (
)

ROOT_URLCONF = 'q_server.urls.staff_production'

INTERNAL_IPS = ('127.0.0.1',)

INSTALLED_APPS += (
)

#==============================================================================
# FastCGI
#==============================================================================
FORCE_SCRIPT_NAME = ''

#==============================================================================
# Email
#==============================================================================
ADMINS = (('system_error', 'yoktys@gmail.com'),)

EMAIL_SUBJECT_PREFIX = '[q-server] '
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25
DEFAULT_FROM_EMAIL = ''
# .. note:: FROMを上記のものに設定するため
SERVER_EMAIL = ''
