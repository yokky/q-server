# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings

urlpatterns = patterns('',
    # apps
    url(r'^demo/', include('demo.urls')),
    url(r'^v1/', include('v1.urls')),
    url(r'^c1/', include('c1.urls')),
    #url(r'^$', include('q_v1.urls')),
    url(r'', include('social_auth.urls')),
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        (r'^__debug__/', include(debug_toolbar.urls)),
    )
