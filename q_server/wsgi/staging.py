import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'q_server.settings.front_staging'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

